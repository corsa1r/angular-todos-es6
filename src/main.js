let angular = require('angular');
let app = angular.module('app', []);

app.controller('TodosController', require('./controllers/TodosController'));
app.service('TodosService', require('./services/TodosService'));