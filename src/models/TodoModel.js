class TodoModel {

    constructor(text) {
        this.text = text;
        this.time = Date.now();
    }

    validate() {
        //Todo cannot be valid without text
        return this.text.length > 0;
    }
}

module.exports = TodoModel;