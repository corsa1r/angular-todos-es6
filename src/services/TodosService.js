class TodosService {

    constructor() {
        //TODO: use $firebaseArray
        this.list = [];
    }

    /**
     * This method adds todo in the list
     * @param {TodoModel}
     */
    add(todo) {
        if(todo.validate()) {
            this.list.push(todo);
        } 
    }

    /**
     * This method marks todo as completed
     * @param {TodoModel}
     */
    complete(todo) {
        todo.completed = true;
    }


    /**
     * This method removes todo from the list
     * @param {TodoModel}
     */
    remove(todo) {
        let index = this.list.indexOf(todo);
        this.list.splice(index, 1);
    }
}

module.exports = TodosService;