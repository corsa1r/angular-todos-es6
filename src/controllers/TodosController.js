let TodoModel = require('../models/TodoModel');

class TodosController {

    constructor(TodosService) {
        //text of the new todo
        this.text = '';
        this.todos = TodosService;
    }

    addTodo() {
        let todo = new TodoModel(this.text);
        this.todos.add(todo);
        this.text = '';
    }

    completeTodo(todo) {
        this.todos.complete(todo);
    }

    removeTodo(todo) {
        this.todos.remove(todo);
    }
}

module.exports = TodosController;