var path = require("path");

module.exports = {
    entry: {
        client: "./src/main.js"
    },
    output: {
        path: path.join(__dirname, "build"),
        filename: "[name].build.js"
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel', // 'babel-loader' is also a valid name to reference
                query: {
                    presets: ['es2015']
                }
            }
        ]
    }
};