# README #

### What is this repository for? ###

* TODO's application using AngularJS and ECMAScript 2016 standard using babel
* 0.5.0

### How do I get set up? ###

* git clone https://corsa1r@bitbucket.org/corsa1r/angular-todos-es6.git
* npm install
* npm run build
* npm run server
* if error occurs when starting a server maybe you need to install ws(WebServer) globally
* npm install ws -g

### Contribution guidelines ###

* If you want you are welcome